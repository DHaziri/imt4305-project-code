### What is this repository for? ###

This is a repository for the Project in the subject IMT4305

### How do I get set up? ###

* Install Visual Studio Community 2019 from: https://visualstudio.microsoft.com/vs/community/
* Make sure python package is included when installing.
* Clone the repository.
* Transfer the ISIC 2020 image data set into the folder "ISIC_2020_Training_Images"
* Open Visual Studio and open the project.
* You should now be able to run  the code

If not: Follow the link to a tutorial on how to install packages and then do:

* pip install numpy
* pip install opencv-python
* pip install matplotlib
* pip install scikit-learn
* pip install scikit-image

https://docs.microsoft.com/en-us/visualstudio/python/tutorial-working-with-python-in-visual-studio-step-05-installing-packages?view=vs-2022

import numpy as np
import cv2
import matplotlib.image as mpimg
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report,confusion_matrix
from skimage.segmentation import chan_vese


def hair_removal(image):

    grayScale = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    
    # kernel for morphologyEx
    kernel = cv2.getStructuringElement(1,(17,17))
    
    # apply MORPH_BLACKHAT to grayScale image
    blackhat = cv2.morphologyEx(grayScale, cv2.MORPH_BLACKHAT, kernel)

    
    # apply thresholding to blackhat
    _,threshold = cv2.threshold(blackhat,10,255,cv2.THRESH_BINARY)
    
    # inpaint with original image and threshold image
    image = cv2.inpaint(image,threshold,1,cv2.INPAINT_TELEA)

    return image
    



def segmentation(image):

    # Converts the image to grayscale
    img_sum = image.sum(axis=2)
    image = img_sum/img_sum.max()

    image = chan_vese(image, mu=0.05, lambda1=1, lambda2=1, max_iter=200,
                   dt=1, init_level_set="checkerboard")

    return image



def classification(trainImg, trainDia, testImg, testDia):
    # Create the models
    model = MLPClassifier()

    #Formating for sklearn
    trainImg = np.array(trainImg, dtype=np.ubyte).reshape(len(trainImg),-1)
    testImg = np.array(testImg, dtype=np.ubyte).reshape(len(testImg),-1)

    # Puts data in the model
    model.fit(trainImg, trainDia)

    # Tests the nodel
    predict_test = model.predict(testImg)


    # Gets and calculates the results
    results = confusion_matrix(testDia, predict_test)

    classAcc = (results[0][0]+results[1][1])/len(testDia)
    errorRate = (results[0][1]+results[1][0])/len(testDia)

    negInstances = 0
    for i in testDia:
        if testDia[i] == 0:
            negInstances += 1 

    if negInstances == 0:
        negInstances = 1

    sensitivity = results[0][0]/predict_test.sum()
    specificity = results[0][1]/negInstances
    posLikelyhood = sensitivity/1 - specificity
    negLikelyhood = 1 - sensitivity/specificity


    # Prints the results
    print("\n\n")
    print("Classification accuracy:\t", classAcc)
    print("Error rate:\t\t\t", errorRate)
    print("Sensitivity:\t\t\t", sensitivity)
    print("Specificity:\t\t\t", specificity)
    print("Positive likelihood:\t\t", posLikelyhood)
    print("Negative likelihood:\t\t", negLikelyhood)

    print("\n")
    print("\t\t\tActual Positive\t\tActual Negative")
    print("Predicted Positive\t", results[0][0], "\t\t\t", results[0][1])
    print("Predicted Negative\t", results[1][0], "\t\t\t", results[1][0])
    print("\n\n")
   



def main():

    # Reads in the metadata
    metadata_file = open("../ISIC_2020_Training_Metadata.csv", "r")
    data = metadata_file.read().splitlines()

    # Limits the amount of pictures work on at the time
    data = data[1:400]

    # Declares all the arrays/matrixes
    imgNames = []
    diagnosis = []
    images = []
    hairOnly = []
    segOnly = []
    allVersions = [[],[],[],[]]

    # Makes the dataset binary
    for line in data:
        imgNames.append(line.split(",")[0])
        if line.split(",")[6] == "melanoma":
            diagnosis.append(1)
        else:
            diagnosis.append(0)       

    # Reads in and resizes each image
    for name in imgNames:
        img  = mpimg.imread("../ISIC_2020_Training_Images/" + name + ".jpg")
        img = cv2.resize(img,(160,120))
        img = np.array(img, dtype=np.ubyte)
        images.append(img)

    # Saves the raw images
    allVersions[0] = images


    # Removes and saves the processed images
    imgTemp = []
    for i in images:
        img = hair_removal(i)
        imgTemp.append(img)

    allVersions[1] = imgTemp
    imgTemp = []


    # Segments the raw images and saves them
    for i in images:
        img = segmentation(i)
        imgTemp.append(img)

    allVersions[2] = imgTemp
    imgTemp = []


    # Segments the processed images and saves them
    for i in allVersions[1]:
        img = segmentation(i)
        imgTemp.append(img)

    allVersions[3] = imgTemp
      
    
    # Empties the arrays to save memory
    images = None
    imgTemp = None

    # Splits the metadata into training and test data
    trainDia = diagnosis[:100]
    testDia = diagnosis[100:]

    # Splits the images into training and test data, then classify them
    # This happens once for every set of images
    for i in allVersions:
        trainImg = i[:100]
        testImg = i[100:]

        classification(trainImg, trainDia, testImg, testDia)



main()


